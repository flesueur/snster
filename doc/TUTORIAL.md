# SNSTER Tutorial


I --- Process of this tutorial
================================

SNSTER is a framework for designing network/system architectures. This tutorial is divided into 2 parts:

* **User - First steps with using SNSTER**. It is about using a previously specified infrastructure (topology, software installed and configured on all containers). On this infrastructure, you will be able to attach to different parts of the infrastructure. In this posture, it is necessary to launch SNSTER (`snster start`) and then to connect to different containers of this infrastructure (`snster display target-sales`, `snster attach target-dmz`, etc.)
* **Designer - Specification of an infrastructure**. This involves specifying an infrastructure:
  * The topology (host names, IPv4/IPv6 addresses, network bridges) (`main.yml` and `group.yml` for each group)
  * Installation and configuration of each system (bash scripts `provision.sh` for each host)
  * Setting up templates to share common aspects (bash scripts `provision.sh` for host templates)


II --- Before getting started
===============================

1. A [general presentation (in English)](https://www.youtube.com/watch?v=waCsmE7BeZs) at ETACS 2021 (video). This video is about MI-LXC but it is quite close.
2. A [video tutorial (in French)](https://snster.tuxlab.net/mi-lxc/media/tuto.mp4) to get started. This video is about MI-LXC but it is quite close.
3. A functional installation:
  * [**Recommended**] A ready-to-use VirtualBox/VMWare VM (about 2.5GB to download and then a 7.2GB VM) ([here](https://snster.tuxlab.net/vms/snster-vm-1.1.0.ova)). You have to connect to it as root/root then, with a terminal:
    * `cd snster/examples/simpleas`
    * `snster start`
    * `snster display target-sales`
  * Or the creation of the VM via Vagrant ([here](INSTALL.md#installation-on-windowsmacoslinux-using-vagrant)). Once Vagrant is finished, you have to connect to the VM as root/root and then, with a terminal :
    * `cd snster/examples/simpleas`
    * `snster create`
    * `snster start`
    * `snster display target-sales`
  * Or the direct installation under Linux: [here](INSTALL.md#installation-on-linux). It requires root privileges and LXC containers will be installed on the host:
    * `git clone https://framagit.org/flesueur/snster.git`
    * `cd snster`
    * `./install.sh`
    * `cd examples/simpleas`
    * `snster create`
    * `snster start`
    * `snster display target-sales`

> To remove a mouse stuck in a Xephyr, Ctrl+Shift



III --- User - First steps with using SNSTER
============================================

In the `/root/snster/examples/simpleas/` directory:
* `snster display target-sales` : to display the graphical desktop of the target-sales host, which can be used as a web browser (as the debian user)
* `snster attach target-dmz` : to get a shell on the target-dmz host (as the root user)
* `snster print` : to display the topology

All hosts have the 2 following accounts : debian/debian and root/root (login/password).

From the target-sales machine, open a browser to connect to `http://www.target.sns`. You get to a Dokuwiki page, which is the expected page hosted on target-dmz. You can also browse websites hosted on the real internet: there is internet connectivity. You can also open wireshark to capture packets.

Open a shell on the target-dmz host (attach command). Install the "nano" package using `apt` and check that it is working. Since there is connectivity, you can install packages. Mousepad is already installed as a graphical text editor.


> If you dowloaded the MILXC VM (milxc-snster-*), you can find a populated topology in /root/mi-lxc. This means you can cd to `/root/mi-lxc` and then execute `snster display target-sales` in this directory. The MI-LXC topology allows to run the same commands on the same hosts as in this tutorial (in fact, the "simpleas" example used here is a subset of the MI-LXC topology).

IV --- Designer - Specification of an infrastructure
====================================================

SNSTER allows the rapid prototyping of an infrastructure. You can either enrich an existing infrastructure (in the examples subdirectory or MI-LXC) or create a new one. In this tutorial we will create a new topology from scratch.

The procedure will be as follows:
* Create and populate a `main.yml` in an empty directory
* Add a group of hosts in a subdirectory
* Provision a host to this group
* Add another group of hosts
* Explore the template mechanism

IV.1 --- Create and populate a `main.yml` in an empty directory
----------------------------------------------------------------

A topology is contained in a folder. Create a "mytopo" folder and create a `main.yml` inside:
```yaml
version: 1

header:
  name: My topology
  comment: A simple topology

config:
  prefix: mt
  nat-bridge: lxcbr0
  default-master: bookworm

masters:
  bookworm:
    backend: lxc
    template: debian
    parameters:
      release: bookworm
      arch: amd64
    family: debian

  alpine:
    backend: lxc
    template: download
    parameters:
      dist: alpine
      release: 3.21
      arch: amd64
    family: alpine

disabled-groups:
  # - groupB
```

Description of what's inside :
* A version tag, currently "1"
* A header with name and description
* Some config parameters:
  * prefix: it will be added (as a prefix) to LXC container names and network bridges names. It is used to prevent conflicts between different topologies
  * nat-bridge: it is the bridge on the host providing internet connectivity, usually lxcbr0
  * default-master: it is the default master used by the hosts
* A list of masters, hosts will derive from these masters
* A list of disabled groups. By default, every subfolders will be used as groups of the topology; this list allows to temporarily disable some of them


IV.2 --- Add a group of hosts in a subdirectory
-----------------------------------------------

Create a subdirectory named ACME (for instance) and create a `mytopo/acme/group.yml` file inside:
```yaml
version: 1

header:
  name: ACME group
  comment: The ACME organization

hosts:
  router:
    master: alpine
    network:
      interfaces:
        eth0:
          bridge: nat-bridge
          ipv4: dhcp
        eth1:
          bridge: acme-lan
          ipv4: 100.80.0.1/16
          ipv6: 2001:db8:80::0:1/48

  dmz:
    network:
      interfaces:
        eth0:
          bridge: acme-lan
          ipv4: 100.80.1.2/16
          ipv6: 2001:db8:80::1:2/48
      gatewayv4: 100.80.0.1
      gatewayv6: 2001:db8:80::0:1
    templates:
      - mailserver:
          domain: acme.sns
      - resolv:
          domain: acme.sns
          ns: 80.67.169.12
```

The 'hosts' part defines 2 hosts, router and dmz:
* router:
  * it uses the "alpine" master (AlpineLinux)
  * it has 2 network interfaces:
    * eth0 is bridged on the LXC NAT (lxcbr0)
    * eth1 is bridged on a new network bridge "acme-lan" with the specified IP configuration
* dmz:
  * since no master is specified, it uses the default (here a debian bullseye)
  * it has a single network interface bound to acme-lan, with a specified gateway (which is the router)
  * it uses 2 templates:
    * it is mailserver, configured with its domainname
    * since it has no dhcp, the resolv template allows to alter the resolv.conf file (80.67.169.12 is an open resolver from FDN)

You can now run `snster create` in the `mytopo` folder. It should create the masters (if not already created) then the acme-router and acme-dmz hosts. Finally, you can `snster attach acme-dmz` and explore this new host !

Templates are a powerful feature to quickly provision and maintain many hosts sharing some similarities. Some templates are provided with SNSTER in the `src/templates` directory:
* bgp router
* authoritative name server, resolver name server, root name server, resolv.conf
* ldap client, NIS client
* mail server, graphical mail client
* sshfs client
* update of CA roots of trust

Customized templates can be added as we will see in the next sections.


IV.3 --- Provision a host in this group
---------------------------------------

To provision the dmz container with a customized script, create the sub-folder `mytopo/acme/dmz/` and write a `provision.sh` script of the type:
```bash
#!/bin/bash
set -e
if [ -z $SNSTERGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

# do something visible
```
* The shebang is mandatory at the beginning and will be used (and a python script, as long as it is called provision.sh, probably works)
* The `set -e` is very strongly recommended (it allows to stop the script as soon as a command returns an error code, and to return an error code in which case the container creation is canceled. Without this `set -e`, the execution continues and the result may surprise you...)
* the _$SNSTERGUARD_ variable is automatically set at runtime in SNSTER, checking it prevents a script from running on your host machine by mistake (ouch!)
* In general, setting the correct directory helps to avoid multiple mess-ups. This folder can contain files to be copied to the new container, etc.

As a good practice in terms of maintenance, you should favour file modifications (with sed, >>, etc.) rather than pure and simple overwriting. Example of a good sed: `sed -i -e 's/Allow from .*/Allow from All/' /etc/apache2/conf-available/dokuwiki.conf`. You can also find in `examples/simpleas/target/ldap/provision.sh` the manipulations allowing to preconfigure (_preseed_) the Debian packages installations.

Once all this is done, we can do `snster destroy acme-dmz` to destroy the acme-dmz host, then `snster create` to create this container with the supplied `provision.sh`. Finally, restart the topology with `snster start` and attache to acme-dmz to check if your provisionning script ran as expected.

> Note that the create operation is lazy, it only creates non-existent containers and will therefore be fast when we just add a new container.

Now that this host is created, we will modify it. Let's add:
* the usage of another template, for example `mailclient` (it is defined in templates/hosts/debian/mailclient, just name it mailclient in the group.yml). This template has 4 parameters, you can see a usage in `examples/simpleas/target/group.yml`. Configure it with fictitious values (just put 'debian' as value for login, this is the name of the local Linux account that will be configured for mail and this account must already exist. The debian account exists and is the one used by default for the display command)
* another action in the provision.sh


To update this container with these changes without rebuilding everything, you need to:
* `snster destroy acme-dmz` # destroys _only_ the acme-dmz
* `snster create` # rebuilds only this missing container
* `snster start` # restarts this new container
* `snster display acme-dmz` # see that claws-mail has been configured by your settings



IV.4 --- Add another group of hosts
-----------------------------------

Create another subdirectory `mytopo/ecorp/` with a new `mytopo/ecorp/group.yml`:
```yaml
version: 1

header:
  name: ECORP group
  comment: The ECORP organization

hosts:
  infra:
    network:
      interfaces:
        eth0:
          bridge: acme-lan
          ipv4: 100.80.2.2/16
          ipv6: 2001:db8:80::2:2/48
      gatewayv4: 100.80.0.1
      gatewayv6: 2001:db8:80::0:1
    templates:
      - resolv:
          domain: acme.sns
          ns: 80.67.169.12
```

You can now `snster create`, `snster start` and `snster attach ecorp-infra`.

Groups can be seen as subparts of the same organization (in this tutorial) but is mostly tailored for describing different AS (Autonomous Systems) interconnected with BGP: such an example can be found in [MI-LXC](https://github.com/flesueur/mi-lxc/tree/master/topology) with several AS using the BGP template for their interconnection.

IV.5 --- Explore the template mechanism
---------------------------------------

SNSTER allows to define personnal host templates. System-wide templates are stored in `/usr/local/share/snster/templates` (in `src/templates` on the git repository); local templates directories can be added to the SNSTER command-line.

Create a `templates` directory somewhere (outside of the `mytopo/` directory). For hosts deriving from the Debian Bullseye master, templates will be looked for in `debian/<templatename>/` subdirectory.

We'll add a host template for greeting in .bashrc, which is identical for many machines. Create a subfolder for this template (`templates/debian/bashgreeting/`), a `provision.sh` script similar to that of a host in this subfolder, and then call this template in the previously created host. You will need to add '-t <template_directory>' to the commandline, for instance `snster -t ../templates create`.
