#!/bin/sh
set -e

echo "Installing SNSTER to /usr/local/{bin,share,lib/python}..."

cp src/snster.py /usr/local/bin/snster
PYVERSION=`python3 -V | cut -d ' ' -f 2 | cut -d '.' -f1-2`
# /usr/local/lib/python$PYVERSION/dist-packages
#cp -ar src/backends/ /usr/lib/python3.9/
cp -ar src/backends/ /usr/local/lib/python$PYVERSION/dist-packages/
mkdir -p /usr/local/share/snster
cp -ar src/masters/ /usr/local/share/snster
cp -ar src/templates/ /usr/local/share/snster
cp src/snster-completion.bash /etc/bash_completion.d/

echo "SNSTER installed !"
