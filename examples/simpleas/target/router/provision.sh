#!/bin/sh
# Transit A with alpine
set -e
if [ -z $SNSTERGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
echo -e '#!/bin/sh\niptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE' > /etc/local.d/iptables.start
chmod +x /etc/local.d/iptables.start
rc-update add local

# keep DHCP on eth0
touch /etc/network/keepdhcp

# Add an unbound dns resolver
apk add unbound
rc-update add unbound

echo -e "server:
	interface: 0.0.0.0
	access-control: 0.0.0.0/0 allow

	local-zone: \"target.sns.\" static
	local-data: \"ns.target.sns. IN A 100.80.0.1\"
  local-data: \"ldap.target.sns. IN A 100.80.0.10\"
  local-data: \"dmz.target.sns. IN A 100.80.1.2\"
	local-data: \"smtp.target.sns. IN A 100.80.1.2\"
	local-data: \"imap.target.sns. IN A 100.80.1.2\"
	local-data: \"filer.target.sns. IN A 100.80.0.6\"
	local-data: \"intranet.target.sns. IN A 100.80.0.5\"
" >> /etc/unbound/unbound.conf
