# SNSTER: System and Network Simulator for hipsTERs

SNSTER (System and Network Simulator for hipsTERs, pronounced "senster") is a rapid prototyping framework for simulating computer infrastructures. It is based on the infrastructure-as-code principle: scripts programmatically generate the target environment. The small memory footprint of LXC combined with differential images allows to run it on modest hardware such as standard laptops.

SNSTER was originally a part of MI-LXC(v1). Since version 2, MI-LXC uses SNSTER from this repository.

# Usage scenarios

* [MI-LXC](https://github.com/flesueur/mi-lxc): Mini-Internet simulator for infosec and networking practical work (intrusion, firewall, IDS, DNS, mail, HTTPS/CA, etc.)
* [Kaz](https://git.kaz.bzh/KAZ/kaz-vagrant/): Simulated environment for the development of [Kaz](https://kaz.bzh), a french associative hoster member of [CHATONS](https://www.chatons.org/)
* PoC, Honeynets, ...

# Examples

There are a few examples in the [examples/](examples/) directory, namely a simple AS and a simple IXP. To try for instance the simple AS:
* Go to the [examples/simpleas/](examples/simpleas/) directory
* Run `snster create`
* Run `snster start`
* Run `snster attach target-sales` (console) or `snster display target-sales` (graphical desktop) to open the target-sales container

SNSTER can also be seen in action in [MI-LXC](https://github.com/flesueur/mi-lxc) with the [pre-installed VM](https://snster.tuxlab.net/mi-lxc/milxc-snster-vm-2.1.0.ova).

# How to use

## Installation

You can either:
* Download the [latest ready-to-run VirtualBox VM with SNSTER](https://www.snster.net/vms/snster-vm-1.1.0.ova) (currently slightly outdated). Login with root/root, then SNSTER is already installed and provisionned in `/root/snster/examples/*/` (i.e., you can directly `snster start`, no need to `snster create`)
* Download the [latest ready-to-run VirtualBox VM with SNSTER+MI-LXC](https://snster.tuxlab.net/mi-lxc/milxc-snster-vm-2.1.0.ova) (up-to-date). Login with root/root, then SNSTER is already installed and example topologies can be found in `/root/snster/examples/`
* Create a [VirtualBox VM using Vagrant](doc/INSTALL.md#installation-on-windowsmacoslinux-using-vagrant). Login with root/root, then SNSTER is already installed. Only example topologies are provisionned.
* Install [directly on your Linux host system](doc/INSTALL.md#installation-on-linux)


Usage
-----

The `snster` script generates and uses containers (as *root*, since it manipulates bridges and lxc commands, more on this [here](doc/INSATALL.md#what-is-done-with-root-permissions-)). It is used as `snster <command>`, with the following commands:

| Command                          | Description |
| -------------------------------- | ----------- |
| `create [name]`                  | Creates the [name] container, defaults to create all containers
| `renet`                          | Renets all the containers
| `destroy [name]`                 | Destroys the [name] container, defaults to destroy all containers
| `destroymaster`                  | Destroys all the master containers
| `updatemaster`                   | Updates all the master containers
| `start`                          | Starts the created infrastructure
| `stop`                           | Stops the created infrastructure
| `attach [user@]<name> [command]` | Attaches a term on \<name> as [user](defaults to root) and executes [command](defaults to interactive shell)
| `display [user@]<name>`          | Displays a graphical desktop on \<name> as [user](defaults to debian)
| `print`                          | Graphically displays the defined architecture
|                                  | (\<arguments> are mandatory and [arguments] are optional)|


Walkthrough Tutorial
--------------------

There is also a [walkthrough tutorial](doc/TUTORIAL.md) for creating new infrastructures.


# License
This software is licensed under AGPLv3: you can freely reuse it as long as you write you use it and you redistribute your modifications. Special licenses with (even) more liberties for public teaching activities can be discussed.
