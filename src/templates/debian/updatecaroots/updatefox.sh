#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

wget http://www.gozilla.milxc/root.crt -O /usr/local/share/ca-certificates/root.crt
/usr/sbin/update-ca-certificates --fresh
