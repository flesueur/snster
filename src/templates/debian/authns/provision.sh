#!/bin/bash
# Root NS template
set -e
if [ -z $SNSTERGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

DEB_VERSION=`cat /etc/debian_version | cut -d'.' -f1`
if [ $DEB_VERSION -eq "11" ] # DEB 11 aka Bullseye
then
	# disable systemd-resolved which conflicts with nsd
	echo "DNSStubListener=no" >> /etc/systemd/resolved.conf
	systemctl stop systemd-resolved
fi

apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y nsd dnsutils

# Iterate zonefiles
IFS=';'
for zonefile in $zonefiles; do
	REALFILE=`find /mnt/snster/ -iname $zonefile`
	if [ -z "$REALFILE" ]; then
		echo "Error in authns template: Cannot find zone $zonefile"
		exit 1
	fi
	DOMAIN=`grep ORIGIN $REALFILE | awk '{print $NF}'`

	cp $REALFILE /etc/nsd/$zonefile

	nsd-checkzone $DOMAIN /etc/nsd/$zonefile

	echo -e "zone:
		name: \"$DOMAIN\"
		zonefile: \"$zonefile\"
	" >> /etc/nsd/nsd.conf

	nsd-checkconf /etc/nsd/nsd.conf

done
