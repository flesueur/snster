#!/bin/bash
# DNS resolver template
set -e
if [ -z $SNSTERGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

DEB_VERSION=`cat /etc/debian_version | cut -d'.' -f1`
if [ $DEB_VERSION -eq "11" ] # DEB 11 aka Bullseye
then
	# disable systemd-resolved which conflicts with nsd
	echo "DNSStubListener=no" >> /etc/systemd/resolved.conf
	systemctl stop systemd-resolved
fi

apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y unbound dnsutils

if [ -z "$roots" ] # require $roots parameter
then
	echo "No DNS roots specified for internal resolver"
	exit 1
fi

> /etc/unbound/root.hints
OLDIFS=$IFS
IFS=';'
for root in $roots; do
	name=`cut -d',' -f1 <<< $root`
	ipv4=`cut -d',' -f2 <<< $root`
	ipv6=`cut -d',' -f3 <<< $root`
	echo -e ".                        3600000      NS    $name.ROOT-SERVERS.NET.
$name.ROOT-SERVERS.NET.      3600000      A     $ipv4
$name.ROOT-SERVERS.NET.      3600000      AAAA     $ipv6" >> /etc/unbound/root.hints
done
IFS=$OLDIFS

# customize unbound config
echo -e "server:
	root-hints: root.hints
" > /etc/unbound/unbound.conf.d/root.conf

# allow reverse DNS for SNSTER internally routables ipv4
for i in {64..127}; do
	echo -e "	local-zone: \"$i.100.in-addr.arpa.\" nodefault" >> /etc/unbound/unbound.conf.d/root.conf
done

# no DNSSEC validation for now
sed -i "s/auto/\#auto/" /etc/unbound/unbound.conf.d/root-auto-trust-anchor-file.conf

# Be an open dns resolver
echo -e "server:
	interface: 0.0.0.0
  access-control: 0.0.0.0/0 allow
	cache-max-ttl: 20
	cache-min-ttl: 10
	cache-max-negative-ttl: 20
" > /etc/unbound/unbound.conf.d/listen.conf

service unbound restart
