#!/bin/bash
# Root NS template
set -e
if [ -z $SNSTERGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

IFS=';'
for root in $roots; do
	name=`cut -d',' -f1 <<< $root`
	ipv4=`cut -d',' -f2 <<< $root`
	ipv6=`cut -d',' -f3 <<< $root`
	echo "Root $name with $ipv4/$ipv6"
done

DEB_VERSION=`cat /etc/debian_version | cut -d'.' -f1`
if [ $DEB_VERSION -eq "11" ] # DEB 11 aka Bullseye
then
	# disable systemd-resolved which conflicts with nsd
	echo "DNSStubListener=no" >> /etc/systemd/resolved.conf
	systemctl stop systemd-resolved
fi

apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y nsd

rmdnssec(){
	file=$1
	sed -i -e '/NSEC.*/d' "$file"
	sed -i -e '/RRSIG.*/d' "$file"
	sed -i -e '/DNSKEY.*/d' "$file"
	sed -i -e '/DS.*/d' "$file"
}

# get root zone
wget "http://www.internic.net/domain/root.zone" -O /etc/nsd/root.zone
rmdnssec "/etc/nsd/root.zone"

# customize root zone
# remove official roots
sed -i -e 's/^\.\s.*NS.*[a-m].root-servers.net.*//' /etc/nsd/root.zone
# add alternative milxc root
for root in $roots; do
	name=`cut -d',' -f1 <<< $root`
	ipv4=`cut -d',' -f2 <<< $root`
	ipv6=`cut -d',' -f3 <<< $root`
	echo -e ".	518400	IN	NS	$name.root-servers.net
$name.root-servers.net	518400	IN	A $ipv4
$name.root-servers.net	518400	IN	AAAA     $ipv6
" >> /etc/nsd/root.zone
done

for tld in $tlds; do
	name=`cut -d',' -f1 <<< $tld`
	ipv4=`cut -d',' -f2 <<< $tld`
	ipv6=`cut -d',' -f3 <<< $tld`
	echo -e "$name.	518400	IN	NS	ns.$name.
ns.$name.	518400	IN	A $ipv4
ns.$name.	518400	IN	AAAA $ipv6" >> /etc/nsd/root.zone
done

nsd-checkzone . /etc/nsd/root.zone

# customize nsd.conf
echo -e "zone:
	name: \".\"
	zonefile: \"root.zone\"
" > /etc/nsd/nsd.conf


# Reverse DNS

## Racine
sed -i -e '/^arpa.*/d' /etc/nsd/root.zone
sed -i -e '/^.\.ns\.arpa.*.*/d' /etc/nsd/root.zone
for root in $roots; do
	name=`cut -d',' -f1 <<< $root`
	ipv4=`cut -d',' -f2 <<< $root`
	ipv6=`cut -d',' -f3 <<< $root`
	echo -e "arpa.	172800  IN      NS      $name.ns.arpa.
$name.ns.arpa.              172800  IN      A       $ipv4
$name.ns.arpa.              172800  IN      AAAA     $ipv6
" >> /etc/nsd/root.zone
done

nsd-checkzone . /etc/nsd/root.zone

## .arpa
wget "https://www.internic.net/domain/arpa.zone" -O /etc/nsd/arpa.zone
rmdnssec "/etc/nsd/arpa.zone"
sed -i -e '/^arpa\.\s.*NS.*[a-m].ns.arpa.*/d' /etc/nsd/arpa.zone
sed -i -e '/^in-addr.*/d' /etc/nsd/arpa.zone
sed -i -e '/^.\.in-addr.*/d' /etc/nsd/arpa.zone
for root in $roots; do
	name=`cut -d',' -f1 <<< $root`
	ipv4=`cut -d',' -f2 <<< $root`
	ipv6=`cut -d',' -f3 <<< $root`
	echo -e "arpa.	172800  IN      NS      $name.ns.arpa.
$name.ns.arpa.              172800  IN      A       $ipv4
$name.ns.arpa.              172800  IN      AAAA     $ipv6
in-addr.arpa.		172800	IN	NS	$name.in-addr-servers.arpa.
$name.in-addr-servers.arpa.              172800  IN      A       $ipv4
$name.in-addr-servers.arpa.              172800  IN      AAAA     $ipv6
" >> /etc/nsd/arpa.zone
done

nsd-checkzone arpa /etc/nsd/arpa.zone

echo -e "zone:
	name: \"arpa.\"
	zonefile: \"arpa.zone\"
" >> /etc/nsd/nsd.conf

## .in-addr.arpa
wget "https://www.internic.net/domain/in-addr.arpa.zone" -O /etc/nsd/in-addr.arpa.zone
rmdnssec "/etc/nsd/in-addr.arpa.zone"
sed -i -e '/^in-addr\.arpa\.\s.*NS.*[a-m].in-addr-servers.arpa.*/d' /etc/nsd/in-addr.arpa.zone
sed -i -e '/^in-addr\.arpa\.\s.*SOA.*[a-m].in-addr-servers.arpa.*/d' /etc/nsd/in-addr.arpa.zone
sed -i -e '/^100.*/d' /etc/nsd/in-addr.arpa.zone
for root in $roots; do
	name=`cut -d',' -f1 <<< $root`
	ipv4=`cut -d',' -f2 <<< $root`
	ipv6=`cut -d',' -f3 <<< $root`
	echo -e "in-addr.arpa.	172800  IN      NS      $name.ns.in-addr.arpa.
$name.ns.in-addr.arpa.              172800  IN      A       $ipv4
$name.ns.in-addr.arpa.              172800  IN      AAAA     $ipv6
100.in-addr.arpa.		172800	IN	NS	$name.100.in-addr.arpa.
$name.100.in-addr.arpa.              172800  IN      A       $ipv4
$name.100.in-addr.arpa.              172800  IN      AAAA     $ipv6
" >> /etc/nsd/in-addr.arpa.zone
done

echo -e "in-addr.arpa.           3600    IN      SOA     b.in-addr-servers.arpa. nstld.iana.org. 2022090676 1800 900 604800 3600" >> /etc/nsd/in-addr.arpa.zone

nsd-checkzone in-addr.arpa /etc/nsd/in-addr.arpa.zone

echo -e "zone:
	name: \"in-addr.arpa.\"
	zonefile: \"in-addr.arpa.zone\"
" >> /etc/nsd/nsd.conf



## 100.in-addr.arpa, generated during install
echo -e "100.in-addr.arpa.           3600    IN      SOA     b.in-addr-servers.arpa. nstld.iana.org. 2022090676 1800 900 604800 3600
" > /etc/nsd/100.in-addr.arpa.zone

if [ ! -z $reverse ]; then
	REALFILE=`find /mnt/snster/ -iname $reverse`
	cat $REALFILE >> /etc/nsd/100.in-addr.arpa.zone
fi

for reversei in $reverses; do
	name=`cut -d',' -f1 <<< $reversei`
	ipv4=`cut -d',' -f2 <<< $reversei`
	ipv6=`cut -d',' -f3 <<< $reversei`
	echo -e "$name.100.in-addr.arpa.	172800	IN	NS	ns.$name.100.in-addr.arpa.
ns.$name.100.in-addr.arpa.	172800	IN	A $ipv4
ns.$name.100.in-addr.arpa.	172800	IN	AAAA $ipv6" >> /etc/nsd/100.in-addr.arpa.zone
done

for root in $roots; do
	name=`cut -d',' -f1 <<< $root`
	ipv4=`cut -d',' -f2 <<< $root`
	ipv6=`cut -d',' -f3 <<< $root`
	echo -e "100.in-addr.arpa.	172800  IN      NS      $name.ns.100.in-addr.arpa.
$name.ns.100.in-addr.arpa.              172800  IN      A       $ipv4
$name.ns.100.in-addr.arpa.              172800  IN      AAAA     $ipv6
" >> /etc/nsd/100.in-addr.arpa.zone
done

for i in {0..63}; do
	dig $i.100.in-addr.arpa NS | grep -v "^;" | grep -v "^\$" | grep -v "^100.in-addr.arpa." | grep "^$i.100.in-addr.arpa." >> /etc/nsd/100.in-addr.arpa.zone;
done || true # some query fail, that's ok

for i in {128..255}; do
	dig $i.100.in-addr.arpa NS | grep -v "^;" | grep -v "^\$" | grep -v "^100.in-addr.arpa." | grep "^$i.100.in-addr.arpa." >> /etc/nsd/100.in-addr.arpa.zone;
done || true # some query fail, that's ok

nsd-checkzone 100.in-addr.arpa /etc/nsd/100.in-addr.arpa.zone

echo -e "zone:
	name: \"100.in-addr.arpa.\"
	zonefile: \"100.in-addr.arpa.zone\"
" >> /etc/nsd/nsd.conf

nsd-checkconf /etc/nsd/nsd.conf

#service nsd restart
