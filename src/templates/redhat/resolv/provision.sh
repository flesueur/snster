#!/bin/sh
# nodhcp template
set -e
if [ -z $SNSTERGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

dnf remove -y NetworkManager
echo "domain $domain" > /etc/resolv.conf
echo "search $domain" >> /etc/resolv.conf
echo "nameserver $ns" >> /etc/resolv.conf
