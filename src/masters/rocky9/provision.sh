#!/bin/bash


set -e
if [ -z $SNSTERGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

dnf update
dnf install -y mkpasswd


usermod -p `mkpasswd --method=sha-512 root` root
