"""
LXC backend interface
"""

from .HostBackend import Host, Master
import lxc
import ipaddress
import os
import sys


def getInterpreter(file):
    script = open(file)
    first = script.readline()
    interpreter = first[2:-1]
    script.close()
    return interpreter


class LxcBackend:
    """
    This class defines a few methods common to LxcHost and LxcMaster
    """
    nextid = 0

    def provision(self, isMaster=False, isRenet=False):
        miname = self.name
        c = self.getContainer()

        if isRenet:
            scriptname = "/renet.sh"
        else:
            scriptname = "/provision.sh"

        if self.folder is not None:
            filesdir = self.folder + scriptname
            if os.path.isfile(filesdir):
                c.start()
                if not isRenet and not c.get_ips(timeout=60):
                    print("Container seems to have failed to start (no IP)")
                    sys.exit(1)

            # filesdir = os.path.dirname(os.path.realpath(sys.modules['__main__'].__file__)) + "/" + path + scriptname
            # if os.path.isfile(filesdir):
                ret = c.attach_wait(lxc.attach_run_command,
                                    ["env"] + ["MILXCGUARD=TRUE", "SNSTERGUARD=TRUE", "HOSTLANG=" + os.getenv("LANG")]
                                    + [getInterpreter(filesdir), "/mnt/snster/" + self.folder + scriptname],
                                    env_policy=lxc.LXC_ATTACH_CLEAR_ENV)
                if ret != 0:
                    print("\033[31mProvisioning of " + self.alias + " failed (" + str(ret) + "), exiting...\033[0m")
                    c.stop()
                    if not isRenet:
                        c.destroy()
                    exit(1)
            else:
                print("No Provisioning script for " + self.alias)
        else:
            print("No Provisioning script for " + self.alias)

        if isinstance(self, LxcMaster):
            c.stop()
            return

        family = self.family
        for templatet in self.templates:
            (template, params), = templatet.items()
            filesdir = params['folder'] + scriptname
            if os.path.isfile(filesdir):
                c.start()
                if not isRenet and not c.get_ips(timeout=60):
                    print("Container seems to have failed to start (no IP)")
                    sys.exit(1)
                args = ["MILXCGUARD=TRUE", "SNSTERGUARD=TRUE", "HOSTLANG=" + os.getenv("LANG")]
                for arg,value in params.items():
                    value = str(value)
                    args.append(arg + "=" + value)
                ret = c.attach_wait(lxc.attach_run_command, ["env"] + args + [getInterpreter(filesdir), "/mnt/snster/" + filesdir], env_policy=lxc.LXC_ATTACH_CLEAR_ENV)
                if ret != 0:  # and ret != 127:
                    print("\033[31mProvisioning of " + self.alias + "/" + template + " failed (" + str(ret) + "), exiting...\033[0m")
                    c.stop()
                    if not isRenet:
                        c.destroy()
                    exit(1)
            else:
                print("No Provisioning script for " + self.alias + "/" + template)

        c.stop()

    def destroy(self):
        print("Destroying " + self.name)
        c = self.getContainer()
        c.stop()
        if not c.destroy():
            print("Failed to destroy the container " + self.name, file=sys.stderr)

    def exists(self):
        c = self.getContainer()
        return c.defined

    def isReady(self):
        c = self.getContainer()
        if not c.get_ips(timeout=60):
            return False
        return True


class LxcHost(LxcBackend, Host):
    """
    This class defines methods to manage LXC hosts
    """
    def __repr__(self):
        return(
            "{" + self.master.backend + ":" + self.name + " | " + self.alias
            + ", group: " + self.group
            + ", master: " + self.master.name
            + ", network: " + str(self.network)
            + ", templates: " + str(self.templates)
            + ", folder: " + str(self.folder)
            + ", id: " + str(self.id)
            + "}")

    def __init__(self, name, group, master, network, templates, folder, alias, lxcbr="lxcbr0"):
        self.name = name
        self.group = group
        #self.backend = "lxc"
        self.lxcbr = lxcbr
        self.master = master
        self.network = network
        self.templates = templates
        self.folder = folder
        self.alias = alias
        #self.folder = "groups/" + group + "/" + name + "/"
        self.family = master.family
        self.id = LxcBackend.nextid
        LxcBackend.nextid += 1

    def getContainer(self):
        return lxc.Container(self.name)

    def create(self):
        c = self.getContainer()
        if c.defined:
            print("Container " + self.alias + " already exists", file=sys.stderr)
            return

        mastercontainer = self.master.getContainer()
        if mastercontainer.defined:
            print("Cloning " + self.alias + " from " + self.master.alias)
            mastercontainer.clone(self.name, flags=lxc.LXC_CLONE_SNAPSHOT)
            self.configure()
            self.provision()
            self.configNet()
        else:
            print("Invalid master container \"" + self.master.alias + "\" for container \"" + self.alias + "\"", file=sys.stderr)
            exit(1)

    def configure(self):
        c = self.getContainer()

        if self.folder is not None:
            filesdir = self.folder.replace(" ", "\\040")
            c.append_config_item(
                "lxc.mount.entry", filesdir + " mnt/snster" + filesdir + " none ro,bind,create=dir 0 0")

        for templatet in self.templates:
            (template, params), = templatet.items()
            filesdir = params['folder'].replace(" ", "\\040")
            c.append_config_item(
                "lxc.mount.entry", filesdir + " mnt/snster" + filesdir + " none ro,bind,create=dir 0 0")

        c.save_config()

    def configNet(self):
        interfaces = self.network["interfaces"]
        gatewayv4 = self.network["gatewayv4"]
        gatewayv6 = self.network["gatewayv6"]
        print("Configuring NICs of " + self.alias + " to " + str(interfaces) + " / gwv4: " + gatewayv4 + " / gwv6: " + gatewayv6)
        c = self.getContainer()
        c.clear_config_item("lxc.net")
        i = 0
        # print(interfaces)
        for iface, params in interfaces.items():
            #k = cnic[0]
            #v = cnic[1]
            c.network.add("veth")
            c.network[i].name = iface
            c.network[i].link = params['bridge']
            c.network[i].flags = "up"


            if 'ipv4' in params:
                ipv4 = params['ipv4']
                if not (ipv4 == 'dhcp'):
                    try:
                        c.network[i].ipv4_address = ipv4
                    except:
                        # c.append_config_item("lxc.network."+str(i)+".ipv4", v)
                        c.append_config_item(
                            "lxc.net." + str(i) + ".ipv4.address", ipv4)
                    try:
                        if ipaddress.ip_address(gatewayv4) in ipaddress.ip_network(ipv4, strict=False):
                            c.network[i].ipv4_gateway = gatewayv4
                    except ValueError:  # gateway is not a valid address, no gateway to set
                        pass

            if 'ipv6' in params:
                ipv6 = params['ipv6']
                if not (ipv6 == 'dhcp'):
                    # compress IPv6 since Lxc does not accept 2001:db8::0:1
                    (ippart, netmask) = ipv6.split('/')
                    ippart = ipaddress.ip_address(ippart)
                    ipv6 = str(ippart) + '/' + netmask
                    try:
                        c.network[i].ipv6_address = ipv6
                    except:
                        # c.append_config_item("lxc.network."+str(i)+".ipv4", v)
                        c.append_config_item(
                            "lxc.net." + str(i) + ".ipv6.address", ipv6)
                    try:
                        if ipaddress.ip_address(gatewayv6) in ipaddress.ip_network(ipv6, strict=False):
                            c.network[i].ipv6_gateway = str(ipaddress.ip_address(gatewayv6))  # recompress ipv6
                    except ValueError:  # gateway is not a valid address, no gateway to set
                        pass

            i += 1

        c.save_config()

    def renet(self):
        c = self.getContainer()

        # clear network config
        c.clear_config_item("lxc.net")
        # c.network.remove(0)

        # add nated bridge
        c.network.add("veth")
        c.network[0].link = self.lxcbr
        c.network[0].flags = "up"
        c.save_config()

        # execs renet.sh
        self.provision(isRenet=True)

        # clear network config
        c.clear_config_item("lxc.net")

        # restores json network config
        self.configNet()

    def isRunning(self):
        c = self.getContainer()
        return c.running

    def start(self):
        c = self.getContainer()
        if c.defined:
            c.start()
        else:
            print("Container " + self.alias + " does not exist ! You need to run \"snster create\"", file=sys.stderr)
            exit(1)

    def stop(self):
        c = self.getContainer()
        if c.defined:
            c.stop()
        else:
            print("Container " + self.alias + " does not exist ! You need to run \"snster create\"", file=sys.stderr)
            exit(1)

    def display(self, user):
        # c.attach(lxc.attach_run_command, ["Xnest", "-sss", "-name", "Xnest",
        # "-display", ":0", ":1"])
        c = self.getContainer()
        cdisplay = ":" + str(self.id + 2)
        hostdisplay = os.getenv("DISPLAY")
        if hostdisplay is None:
            print("No $DISPLAY environment variable set, unable to diplay a container", file=sys.stderr)
            exit(1)
        os.system("xhost local: >/dev/null 2>&1")
        command = "DISPLAY=" + hostdisplay + " Xephyr -title \"Xephyr " + user + "@" + self.alias + "\" -br -ac -dpms -s 0 -no-host-grab -resizeable " + cdisplay + " 2>/dev/null & \
            export DISPLAY=" + cdisplay + " ; \
            while ! setxkbmap -query 1>/dev/null 2>/dev/null ; do sleep 0.1s ; done ; \
            lxsession 2>/dev/null & \
            while ! pidof lxpanel 1>/dev/null ; do sleep 0.1s ; done ; \
            setxkbmap -display " + hostdisplay + " -print | xkbcomp - " + cdisplay + " 2>/dev/null"
        # xkbcomp " + str(hostdisplay) + " :" + str(displaynum)
        # setxkbmap " + getxkbmap()
        # to set a cookie in xephyr : xauth list puis ajout -cookie
        # https://unix.stackexchange.com/questions/313234/how-to-launch-xephyr-without-sleep-ing

        # print(command)
        # c.attach(lxc.attach_run_command, ["/usr/bin/pkill", "-f", "Xephyr", "-u", user], env_policy=lxc.LXC_ATTACH_CLEAR_ENV)
        c.attach(
            lxc.attach_run_command,
            ["/bin/su", "-l", "-c", command, user],
            env_policy=lxc.LXC_ATTACH_CLEAR_ENV)

    # Xnest and firefox seem incompatible with kernel.unprivileged_userns_clone=1 (need to disable the multiprocess)
    # Xephyr : can try -host-cursor

    def attach(self, user, run_command):
        if run_command is not None:
            command = " ".join(run_command)
            run_command = ["env", "TERM=" + os.getenv(
                "TERM"), "/bin/su", "-c", command, "-", user]
        else:
            run_command = [
                "env", "TERM=" + os.getenv("TERM"), "/bin/su", "-", user]
        lxccontainer = self.getContainer()
        print("\033]0;" + user + "@" + self.alias  + "\007")
        lxccontainer.attach_wait(lxc.attach_run_command, run_command, env_policy=lxc.LXC_ATTACH_CLEAR_ENV)
        print("\033]0;\007")


class LxcMaster(LxcBackend, Master):
    """
    This class defines methods to manage LXC masters
    """
    def __repr__(self):
        return("{Master " + self.backend + ":" + self.name + " | " + self.alias + ", " + self.folder + "}")

    def __init__(self, name, parameters, template, family, folder, lxcbr, alias):
        self.name = name
        self.alias = alias
        self.backend = "lxc"
        self.lxcbr = lxcbr
        self.isMaster = True
        self.template = template
        self.parameters = parameters
        self.family = family
        self.folder = folder
        self.id = LxcBackend.nextid
        LxcBackend.nextid += 1

    def getContainer(self):
        return lxc.Container(self.name)

    def create(self):
        print("Creating master " + self.alias)
        c = self.getContainer()
        if c.defined:
            print("Master container " + self.alias + " already exists", file=sys.stdout)
            return c

        # can add flags=LXC_CREATE_QUIET to reduce verbosity
        if not c.create(template=self.template, args=self.parameters):
            print("Failed to create the container rootfs", file=sys.stderr)
            sys.exit(1)
        self.configure()
        self.provision(isMaster=True)
        print("Master " + self.alias + " created successfully")
        return c

    def update(self):
        print("Updating master " + self.alias)
        c = self.getContainer()
        if c.defined:
            print("Master container " + self.alias + " exists, updating...", file=sys.stdout)
            path = "masters/" + self.alias
            filesdir = os.path.dirname(os.path.realpath(sys.modules['__main__'].__file__)) + "/" + path + "/update.sh"
            if not os.path.isfile(filesdir):
                print("\033[31mNo update script for master !\033[0m", file=sys.stderr)
                c.stop()
                exit(1)

            c.start()
            if not c.get_ips(timeout=60):
                print("Container seems to have failed to start (no IP)", file=sys.stderr)
                sys.exit(1)

            ret = c.attach_wait(
                lxc.attach_run_command,
                ["env"] + ["SNSTERGUARD=TRUE"] + [getInterpreter(filesdir), "/mnt/lxc/" + path + "/update.sh"],
                env_policy=lxc.LXC_ATTACH_CLEAR_ENV)

            if ret != 0:
                print("\033[31mUpdating of master failed (" + str(ret) + "), exiting...\033[0m", file=sys.stderr)
                c.stop()
                exit(1)

            c.stop()
            print("Master " + self.alias + " updated successfully")
        return c

    def configure(self):
        # c = lxc.Container(master)
        c = self.getContainer()
        c.clear_config_item("lxc.net")
        # c.network.remove(0)
        c.network.add("veth")
        c.network[0].link = self.lxcbr
        c.network[0].flags = "up"
        c.append_config_item(
            "lxc.mount.entry", "/tmp/.X11-unix tmp/.X11-unix none ro,bind,create=dir 0 0")
        # filesdir = os.path.dirname(os.path.realpath(sys.modules['__main__'].__file__))
        filesdir = self.folder.replace(" ", "\\040")
        c.append_config_item(
            "lxc.mount.entry", filesdir + " mnt/snster" + filesdir + " none ro,bind,create=dir 0 0")
        try:  # AppArmor is installed and must be configured
            c.get_config_item("lxc.apparmor.profile")
            # may be aa_profile sometimes ?
            c.append_config_item("lxc.apparmor.profile", "unconfined")
        except:  # AppArmor is not installed and must not be configured
            pass
        try:  # AppArmor is installed and must be configured
            c.get_config_item("lxc.aa_profile")   # may be aa_profile sometimes ?
            c.append_config_item("lxc.aa_profile", "unconfined")
        except:  # AppArmor is not installed and must not be configured
            pass

        c.save_config()
