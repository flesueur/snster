#!/usr/bin/python3

import argparse
import sys
import os
import pprint
from backends import LxcBackend, DynamipsBackend
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

# globals
topoconf = "main.yml"
topodir = "."
installdir = "/usr/local/share/snster"
templatesdir = ["templates", os.path.dirname(os.path.realpath(sys.modules['__main__'].__file__)) + "/templates", installdir + "/templates"] # cwd + snster folder
mastersdir = ["masters", os.path.dirname(os.path.realpath(sys.modules['__main__'].__file__)) + "/masters", installdir + "/masters"] # cwd + snster folder
verbose = False

prefix = "sr"
lxcbr = "lxcbr0"
sep = "-"
default_master = ""

masters = {}  # dict of masters
disabled_groups = set() # List of disabled groups
groups = set() # List of enabled groups
hosts = {} # dict of hosts specs
containers = {} # dict of containers
bridges = set() # set of bridges


def flushArp():
    os.system("ip neigh flush dev " + lxcbr)

def getGlobals(data):
    global lxcbr, prefix, default_master
    lxcbr = data["config"]["nat-bridge"]
    prefix = data["config"]["prefix"]
    default_master = data["config"]["default-master"]
    return

def getMasters(data):
    global masters
    for mastername in data["masters"]:
        master = data["masters"][mastername]
        folder = resolveFolder(mastersdir, mastername)
        if master['backend'] == "lxc":
            masterc = LxcBackend.LxcMaster(name="sr" + sep + "masters" + sep + mastername, parameters=master['parameters'], folder=folder,
                                            template=master['template'], family=master['family'], lxcbr = lxcbr, alias = mastername)
            masters[mastername] = masterc
        elif master['backend'] == "dynamips":
            masterc = DynamipsBackend.DynamipsMaster(name="sr" + sep + "masters" + sep + mastername, rom=master['rom'], family=master['family'])
            masters[mastername] = masterc
        else:
            print("Backend " + master['backend'] + " not supported, exiting", file=sys.stderr)
            exit(1)
    return

def getDisabledGroups(data):
    global disabled_groups
    if data["disabled-groups"]:
        for group in data["disabled-groups"]:
            disabled_groups.add(group)
    return

def getGroups():
    global groups, disabled_groups
    #groups = set(os.listdir(topoconf))
    groups = set([d.name for d in os.scandir(topodir) if d.is_dir()])
    groups = groups.difference(disabled_groups)

def getHosts():
    global hosts

    for group in groups:
        yaml_file = open(topodir + "/" + group + "/group.yml")
        yaml_content = load(yaml_file, Loader=Loader)

        if verbose:
            print("local: ")
            for key, value in yaml_content.items():
                print(f"{key}: {value}")

        for host, params in yaml_content["hosts"].items():
            # params = yaml_content["hosts"][host]
            if "master" in params.keys():
                master = masters[params["master"]]
            else:  # use default master
                master = masters[default_master]

            if "templates" not in params:
                params["templates"] = []

            # for template in params["templates"]:
            #     print("")
            #     (ttemplate, tparams), = template.items()
            #     print(tparams)
            #     print("\n\n")
            #     if tparams is None:
            #         print("not ok")
            #         # template[ttemplate] = {}

            network = cleanNetwork(params["network"])

            if master.backend == "lxc":
                # os.path.abspath(topodir + "/" + group + "/" + host + "/") + "/"
                folder = resolveFolder([topodir + "/" + group], host, fatal=False)
                newcont = LxcBackend.LxcHost(name=prefix + sep + group + sep + host, group=group, folder=folder, master=master, network=network, templates=resolveTemplates(params["templates"], master.family), alias=group + sep + host, lxcbr=lxcbr)
                hosts[group + sep + host] = newcont
            elif master.backend == "dynamips":
                newcont = DynamipsBackend.DynamipsHost(name=prefix + sep + group + sep + host, nics=nics[container], templates=mitemplates[container], master=master, folder=folders[container], alias=group + sep + host)
                hosts[group + sep + host] = newcont
            else:
                print("Backend " + master.backend + " not supported, exiting", file=sys.stderr)
                exit(1)
    return

def cleanNetwork(network):
    for interface, params in network["interfaces"].items():
        if params["bridge"] == "nat-bridge":
            params["bridge"] = lxcbr
        else:
            params["bridge"] = prefix + sep + params["bridge"]

    if 'gatewayv4' not in network:
        network["gatewayv4"] = 'None'

    if 'gatewayv6' not in network:
        network["gatewayv6"] = 'None'

    return network


def getBridges():
    global bridges
    for host in hosts:
        for interface, params in hosts[host].network["interfaces"].items():
            if params["bridge"] != lxcbr: #"nat-bridge":
                bridges.add(params["bridge"])
    return


def createMasters():
    print("Creating masters")
    for master in masters.values():
        if not master.exists():
            flushArp()
            master.create()
        else:
            print("Master container " + master.alias + " already exists", file=sys.stderr)
    print("Masters created successfully !")


def updateMasters():
    print("Updating masters")
    for master in masters.values():
        master.update()
    print("Masters updated successfully !")


def createInfra():
    createMasters()
    for host in hosts.values():
        if not host.exists():
            flushArp()
            host.create()
        else:
            print("Host " + host.alias + " already exists", file=sys.stderr)
    print("Infrastructure created successfully !")


def renetInfra():
    for host in hosts.values():
        if not host.exists():
            print("Host " + host.alias + " does not exist", file=sys.stderr)
            exit(1)
        else:
            flushArp()
            host.renet()
    print("Infrastructure reneted successfully !")


def destroyInfra():
    for host in hosts.values():
        host.destroy()


def destroyMasters():
    for master in masters.values():
        master.destroy()


def increaseInotify():
    print("Increasing inotify kernel parameters through sysctl")
    os.system("sysctl fs.inotify.max_queued_events=1048576 fs.inotify.max_user_instances=1048576 fs.inotify.max_user_watches=1048576 1>/dev/null")


def startInfra():
    createBridges()
    increaseInotify()
    for host in hosts:
        print("Starting " + host)
        hosts[host].start()
# Commented out since it quite works but is too long for each container already ready. Will have to be parallelized
#    print("Waiting for containers to be ready...")
#    for host in reversed(hosts):
#        host.isReady()


def stopInfra():
    for host in hosts:
        print("Stopping " + host)
        hosts[host].stop()
    deleteBridges()


def createBridges():
    print("Creating bridges")
    # os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")
    for bridge in bridges:
        os.system("brctl addbr " + bridge)
        os.system("ip link set " + bridge + " up")
        os.system("iptables -A FORWARD -i " + bridge + " -o " + bridge + " -j ACCEPT")


def deleteBridges():
    print("Deleting bridges")
    # os.system("echo 0 > /proc/sys/net/ipv4/ip_forward")
    for bridge in bridges:
        os.system("ip link set " + bridge + " down")
        os.system("brctl delbr " + bridge)
        os.system("iptables -D FORWARD -i " + bridge + " -o " + bridge + " -j ACCEPT")


def printgraph():
    import pygraphviz as pgv
    import tempfile
    from PIL import Image
    G2 = pgv.AGraph()
    G2.graph_attr['overlap'] = "false"
    G2.node_attr['style'] = "filled"

    for host in hosts:
        G2.add_node("c" + host, colorscheme='brbg9', color='2', shape='box', label=host, fontsize=10, height=0.2)

    for bridge in bridges:
        G2.add_node("b" + bridge[len(prefix)+1:], colorscheme='brbg9', color='4', label=bridge[len(prefix)+1:], fontsize=10, height=0.1)

    G2.add_node("b" + lxcbr, colorscheme='brbg9', color='6', label=lxcbr, fontsize=10, height=0.2)
    # 69A2B0, A1C084, FFCAB1, 659157, E05263

    for host in hosts:
        for interface, params in hosts[host].network["interfaces"].items():
            label = ""
            if 'ipv4' in params.keys():
                label += params['ipv4']
            if 'ipv6' in params.keys():
                label += "\n" + params['ipv6']
            if params["bridge"] == lxcbr:
                bridgename = lxcbr
            else:
                bridgename = params["bridge"][len(prefix)+1:]
            G2.add_edge("c" + host, "b" + bridgename, label=label, fontsize=8)  # with IPs

    # G2.write("test.dot")
    # G2.draw("test.png", prog="neato")
    fout = tempfile.NamedTemporaryFile(suffix=".png", delete=True)
    G2.draw(fout.name, prog="sfdp", format="png")
    Image.open(fout.name).show()



def listHosts():
    return ", ".join(host for host in hosts.keys())


def terminal_size():
    import fcntl
    import termios
    import struct
    h, w, hp, wp = struct.unpack('HHHH', fcntl.ioctl(0, termios.TIOCGWINSZ, struct.pack('HHHH', 0, 0, 0, 0)))
    return w, h


def debugData(name, data):
    if verbose:
        w, h = terminal_size()
        pp = pprint.PrettyPrinter(indent=2, width=w)
        print(name + ": ")
        pp.pprint(data)
        print("\n\n")

def resolveFolder(flist, name, fatal=True):
    for f in flist:
        folder = f + "/" + name + "/"
        #print("looking for " + folder)
        #t = os.path.dirname(f + "/" + name)
        if os.path.isdir(folder):
            #print("found !")
            return os.path.abspath(folder) + "/"

    if fatal:
        print("Unfound folder for " + name + " in " + str(flist), file=sys.stderr)
        exit(1)
    return None

def resolveTemplates(templates, family):
    for templatet in templates:
        (template, params), = templatet.items()
        if params is None:
            templatet[template] = {}
        # print("processing template " + template)
        folder = resolveFolder(templatesdir, family + "/" + template)
        templatet[template]['folder'] = folder
    # print("\nDone:\n" + str(templates))
    return templates

def createArgParser():
    parser = argparse.ArgumentParser(description='Launches SNSTER, the System and Network Simulator for hipsTERs')#, epilog="Container names are: "+listHosts())
    parser.add_argument("-c", type=str, help='config folder, defaults to .', dest="config", default=".") # -s setup/simulation ? Prendre par défaut le dossier courant, -s eut donner un dossier -> le main.json ou un fichier qui sera le global. Peut être fix par une variable d'ENV. Trouver comment lire avant l'usage. https://stackoverflow.com/questions/36738309/python-argparse-print-epilog-only-when-verbose pour changer l'épilogue après coup
    parser.add_argument("-t", type=str, help='add a templates folder', dest="templatesdir", nargs=1, action='append')
    parser.add_argument("-m", type=str, help='add a masters folder', dest="mastersdir", nargs=1, action='append')
    parser.add_argument("-v", help='verbose', dest="verbose", action="store_true")

    #parser.add_argument("command", type=str, choices=['create', 'destroy', 'start', 'stop','attach', 'display'], help='command')
    subparsers = parser.add_subparsers(help='sub-command help', dest="command")

    parser_attach = subparsers.add_parser('create', help='creates the containers')
    parser_attach.add_argument('name', type=str, help='container name (optional, default is to create all the containers)', nargs='?')
    #parser_attach.add_argument('-n', type=str, help='container name (optional, default is to create all the containers)', dest="name")

    parser_attach = subparsers.add_parser('renet', help='renets the containers')
    parser_attach.add_argument('name', type=str, help='container name (optional, default is to renet all the containers)', nargs='?')
    #parser_attach.add_argument('-n', type=str, help='container name (optional, default is to renet all the containers)', dest="name")

    parser_attach = subparsers.add_parser('destroy', help='destroys the containers')
    parser_attach.add_argument('name', type=str, help='container name (optional, default is to destroy all the containers)', nargs='?')
    #parser_attach.add_argument('-n', type=str, help='container name (optional, default is to destroy all the containers)', dest="name")

    parser_attach = subparsers.add_parser('updatemasters', help='updates all the master containers')
    parser_attach = subparsers.add_parser('destroymasters', help='destroy all the master containers')

    parser_attach = subparsers.add_parser('start', help='starts the created infrastructure')
    parser_attach.add_argument('name', type=str, help='container name (optional, default is to start all the containers)', nargs='?')
    #parser_attach.add_argument('-n', type=str, help='container name (optional, default is to start all the containers)', dest="name")

    parser_attach = subparsers.add_parser('stop', help='stops the created infrastructure')
    parser_attach.add_argument('name', type=str, help='container name (optional, default is to stop all the containers)', nargs='?')
    #parser_attach.add_argument('-n', type=str, help='container name (optional, default is to stop all the containers)', dest="name")

    parser_attach = subparsers.add_parser('attach', help='attaches a term to a container')
    parser_attach.add_argument('name', type=str, help='container name or login@container:command', nargs='?')
    #parser_attach.add_argument('-n', type=str, help='container name or login@container:command', dest='name', required=False)
    parser_attach.add_argument('-l', type=str, help='login (optional, default is root)', dest='login', required=False, default="root")
    parser_attach.add_argument('-x', type=str, help='command (optional, default is an interactive shell)', dest='exec', required=False, nargs='*')

    parser_attach = subparsers.add_parser('display', help='displays a graphical desktop of a container')
    parser_attach.add_argument('name', type=str, help='container name or login@container', nargs='?')
    #parser_attach.add_argument('-n', type=str, help='container name or login@container', dest='name', required=False)
    parser_attach.add_argument('-l', type=str, help='login (optional, default is debian)', dest='login', required=False, default="debian")

    parser_attach = subparsers.add_parser('print', help='graphically displays the defined architecture')
    parser_attach = subparsers.add_parser('list', help='lists the defined containers')

    return parser


if __name__ == '__main__':
    parser = createArgParser()
    args = parser.parse_args()
    verbose = args.verbose

    if (verbose):
        print("Arguments are: ", args)

    topodir = args.config

    try:
        for dir in args.templatesdir[::-1]:
            templatesdir.insert(0, dir[0])
    except:
        pass
    debugData("Templates Dirs", templatesdir)

    try:
        for dir in args.mastersdir[::-1]:
            mastersdir.insert(0, dir[0])
    except:
        pass
    debugData("Masters Dirs", mastersdir)

    # print(resolveFolder(templatesdir, "debian/bgprouter"))
    # mastersdir = ["masters"]
    try:
        yaml_file = open(topodir + "/" + topoconf)
        yaml_content = load(yaml_file, Loader=Loader)
    except:
        exit("No main.yaml found in current or specified directory")

    if verbose:
        print("Config: ")
        for key, value in yaml_content.items():
            print(f"{key}: {value}")

    getGlobals(yaml_content)
    getMasters(yaml_content)
    debugData("Masters", masters)
    getDisabledGroups(yaml_content)
    debugData("Disabled groups", disabled_groups)
    getGroups()
    debugData("Groups", groups)
    getHosts()
    debugData("Hosts", hosts)
    getBridges()
    debugData("Bridges", bridges)

    if os.geteuid() != 0:
        exit("You need to have root privileges to run this script.\nExiting.")

    flushArp()

    if (args.command == "create"):
        if args.name is not None:
            host = hosts[args.name]
            if host is None:
                print("Unexisting container " + args.name + ", valid containers are " + listHosts(), file=sys.stderr)
                exit(1)
            host.create()
        else:
            createInfra()
    elif (args.command == "destroy"):
        if args.name is not None:
            try:
                host = hosts[args.name]
            except KeyError:
                print("Unexisting container " + args.name + ", valid containers are " + listHosts(), file=sys.stderr)
                exit(1)
            host.destroy()
        else:
            answer = input("Are you sure you want to destroy the whole infrastructure? [y/n] ")
            if answer.lower() in ["y","yes"]:
                destroyInfra()
            else:
                pass
    elif (args.command == "start"):
        startInfra()
    elif (args.command == "stop"):
        stopInfra()
    elif (args.command == "attach"):
        user_container = args.name.split("@")
        if len(user_container) == 2:
            user = user_container[0]
            container = user_container[1]
        else:
            user = "root"
            container = user_container[0]

        if args.exec is not None:
            command = args.exec
        else:
            command = None

        host = hosts.get(container)
        if host is None:
            print("Unexisting container " + container + ", valid containers are " + listHosts(), file=sys.stderr)
            exit(1)
        if not host.isRunning():
            print("Container " + container + " is not running. You need to run \"snster start\" before attaching to a container", file=sys.stderr)
            exit(1)
        print("\n  Attaching to " + host.alias + " as user " + user + "\n")
        host.attach(user, command)

    elif (args.command == "display"):
        user_container = args.name.split("@")
        if len(user_container) == 2:
            user = user_container[0]
            container = user_container[1]
        else:
            user = "debian"
            container = user_container[0]

        host = hosts.get(container)
        if host is None:
            print("Unexisting container " + container + ", valid containers are " + listHosts(), file=sys.stderr)
            exit(1)
        if not host.isRunning():
            print("Container " + container + " is not running. You need to run \"snster start\" before attaching to a container", file=sys.stderr)
            exit(1)
        print("\n  Displaying " + host.alias + " as user " + user + "\n")
        host.display(user)
    elif (args.command == "updatemasters"):
        updateMasters()
    elif (args.command == "destroymasters"):
        destroyMasters()
    elif (args.command == "print"):
        printgraph()
    elif (args.command == "renet"):
        renetInfra()
    elif (args.command == "list"):
        print("Container names are: ", end='')
        print(listHosts()+".")#+"\n")
    else:
        parser.print_help()
        print("\nContainer names are: ", end='')
        print(listHosts()+".")#+"\n")
        exit(1)
